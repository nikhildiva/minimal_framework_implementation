package tests;

import common.page.LoginPage;
import common.page.Scenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testData.LoginTestData;

import java.util.HashMap;

public class LoginWithInvalidUser implements LoginTestData {
	private HashMap<String, String> credentials;
	private WebDriver driver;

	@BeforeMethod
	public void Setup() {
		driver = new FirefoxDriver();
		credentials = getLoginData();
	}

	@Test(description = "logging into portal")
	public void loginTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginWith(credentials);
		loginPage.validateLogin(Scenario.INVALID_USER);
	}

	@AfterMethod
	void tearDown() {
		driver.quit();
	}

	@Override
	public HashMap<String, String> getLoginData() {
		HashMap<String, String> credentials = LoginTestData.super.getLoginData();

		credentials.put("userName", "invalid_user");
		return credentials;
	}
}
