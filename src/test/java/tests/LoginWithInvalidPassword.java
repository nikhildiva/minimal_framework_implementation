package tests;

import common.page.LoginPage;
import common.page.Scenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testData.LoginTestData;

import java.util.HashMap;

public class LoginWithInvalidPassword implements LoginTestData {
	private HashMap<String, String> credentials;
	private WebDriver driver;

	@BeforeMethod
	public void Setup() {
		driver = new PhantonJsDriver();
		credentials = getLoginData();
	}

	@Test(description = "logging into portal")
	public void loginTest() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginWith(credentials);
		loginPage.validateLogin(Scenario.INVALID_PASS);
	}

	@AfterMethod
	void tearDown() {
		driver.quit();
	}

	@Override
	public HashMap<String, String> getLoginData() {
		HashMap<String, String> credentials = LoginTestData.super.getLoginData();

		credentials.put("passWord", "invalid_password");
		return credentials;
	}
}
