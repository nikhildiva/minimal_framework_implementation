package testData;

import java.util.HashMap;

public interface LoginTestData {

	default HashMap<String, String> getLoginData() {
		return new HashMap<String, String>() {{
			put("userName", "validUserName");
			put("passWord", "validPassword");
		}};
	}
}
