package common.page;

public enum Scenario {
	VALID_LOGIN("Login success"),
	INVALID_PASS("Login failed"),
	INVALID_USER("Invalid User");

	Scenario(String message) {
		this.message = message;
	}

	String message;

}
